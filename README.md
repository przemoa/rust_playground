# Rust playground

Just playing around with RUST, and also a startup project with some useful stuff, including:
 - logging with `log4rs` to console and file
 - `AppConfig` - class with configuration for the app
 - threaded module device
 - request-response queue
 - binary serialization
