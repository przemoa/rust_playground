
use std::{sync::mpsc, time::Duration};


pub struct SendReceiveChannel<T1, T2> {
    tx_request: mpsc::Sender<T1>,
    rx_reply: mpsc::Receiver<T2>,
}

impl<T1, T2> SendReceiveChannel<T1, T2> {
    pub fn send(&mut self, v: T1) -> Result<(), String> {
        while let Ok(_) = self.rx_reply.try_recv() {
            log::info!("Discarding old reply");
        }

        self.tx_request.send(v).map_err(|e| e.to_string())
    }

    pub fn receive(&mut self, timeout: Duration) -> Result<T2, String> {
        self.rx_reply
            .recv_timeout(timeout)
            .map_err(|e| e.to_string())
    }
}

pub struct ReceiveSendChannel<T1, T2> {
    rx_request: mpsc::Receiver<T1>,
    tx_reply: mpsc::Sender<T2>,
}

impl<T1, T2> ReceiveSendChannel<T1, T2> {
    pub fn receive(&mut self, timeout: Duration) -> Result<T1, String> {
        self.rx_request
            .recv_timeout(timeout)
            .map_err(|e| e.to_string())
    }

    pub fn send(&mut self, v: T2) -> Result<(), String> {
        self.tx_reply.send(v).map_err(|e| e.to_string())
    }
}

/// Create a pair of channels, one for sending and receiving, second for receiving and sending.
///
/// Together they allow to:
///  - send request from thread 1
///  - receive request in thread 2
///  - send reply from thread 2
///  - receive reply in thread 1
pub fn create<T1, T2>() -> (SendReceiveChannel<T1, T2>, ReceiveSendChannel<T1, T2>) {
    let (tx_request, rx_request) = mpsc::channel();
    let (tx_reply, rx_reply) = mpsc::channel();
    let src = SendReceiveChannel {
        tx_request,
        rx_reply,
    };
    let rsc = ReceiveSendChannel {
        rx_request,
        tx_reply,
    };
    (src, rsc)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::thread;

    #[test]
    fn normal_usage() {
        let (mut txrx, rxtx) = create();

        let thread_handle = thread::spawn(move || {
            let mut rxtx = rxtx;
            for _ in 1..10 {
                let v = rxtx
                    .receive(Duration::from_secs(100))
                    .expect("Failed to receive");
                let s = v * v;
                println!("Received: {v}; sending: {s}");
                rxtx.send(s).expect("Failed to send");
            }
        });

        for i in 1..10 {
            txrx.send(i).expect("0Failed to send");
            let v = txrx.receive(Duration::from_secs(100)).unwrap();
            println!("Sent: {i}; Received: {v}");
        }

        thread_handle.join().unwrap();
    }
}
