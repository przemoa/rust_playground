use std::{
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
    thread,
};

use crate::app_config::AppConfig;

pub struct Device {
    params: DeviceWorkerParams,
    task_handle: Option<thread::JoinHandle<()>>,
}

impl Device {
    pub fn new(config: &AppConfig) -> Self {
        Self {
            params: DeviceWorkerParams {
                config: config.clone(),
                should_stop: Arc::new(AtomicBool::new(false)),
            },
            task_handle: None,
        }
    }

    pub fn start(&mut self) {
        log::info!("Starting Device...");

        let params = self.params.clone();
        self.task_handle = Some(thread::spawn(move || {
            let mut worker = DeviceWorker::new(params);
            worker.run();
        }));
    }

    pub fn stop(&mut self) {
        log::info!("Stopping Device...");
        self.params.should_stop.store(true, Ordering::SeqCst);
        self.task_handle
            .take()
            .expect("Called stop on non-running thread")
            .join()
            .expect("Could not join thread");
    }

    // put here the public function, which will communicate with the DeviceWorker via channels
    // e.g. with futures/promises send via channel
}

#[derive(Clone, Debug)]
struct DeviceWorkerParams {
    config: AppConfig,
    should_stop: Arc<AtomicBool>,
}

struct DeviceWorker {
    params: DeviceWorkerParams,
}

impl DeviceWorker {
    pub fn new(params: DeviceWorkerParams) -> Self {
        Self { params: params }
    }

    fn _perform(&self) {
        log::info!("Performing DeviceWorker task...");
        // do something
    }

    pub fn run(&mut self) {
        log::info!("DeviceWorker started");

        while !self.params.should_stop.load(Ordering::SeqCst) {
            // here goes some code that runs periodically to monitor the Device state
            // e.g. sending some heartbeat
            // ...

            std::thread::sleep(std::time::Duration::from_millis(1000));
        }
        log::info!("DeviceWorker stopped");
    }
}
