use std::{path::Path, time::SystemTime};

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct SomeData {
    pub id: u32,
    pub creation_time: SystemTime,
    pub img: Option<Vec<u8>>,
}

impl SomeData {
    pub fn new(img: &[u8]) -> Self {
        Self {
            id: 0,
            creation_time: SystemTime::now(),
            img: Some(img.to_vec()),
        }
    }

    pub fn save_to_file(&self, path: &Path) -> Result<(), String> {
        let file =
            std::fs::File::create(path).map_err(|e| format!("Failed to create file: '{e}'"))?;
        let mut writer = std::io::BufWriter::new(file);
        // let serialized = bincode::serialize(&unserializable_item).unwrap();
        bincode::serialize_into(&mut writer, self)
            .map_err(|e| format!("Failed to serialize: '{e}'"))
    }

    pub fn load_from_file(path: &Path) -> Result<Self, String> {
        let file = std::fs::File::open(path).map_err(|e| format!("Failed to open file: '{e}'"))?;
        let mut reader = std::io::BufReader::new(file);
        bincode::deserialize_from(&mut reader).map_err(|e| format!("Failed to deserialize: '{e}'"))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::path::PathBuf;
    use tempfile::tempdir;

    #[test]
    fn write_to_file_and_read() {
        let dir = tempdir().expect("Failed to crate temp dir!");
        let path = PathBuf::from(dir.path()).join("test.bin");
        let data = SomeData::new(&[1, 2, 3, 4, 5]);

        data.save_to_file(&path).expect("Failed to save to file!");
        let loaded = SomeData::load_from_file(&path).expect("Failed to load from file!");

        assert_eq!(data, loaded);
    }

    #[test]
    fn write_error() {
        let path = PathBuf::from("/root/definitively/forbidden/path");
        let data = SomeData::new(&[1, 2, 3, 4, 5]);
        let res = data.save_to_file(&path);

        assert!(res.is_err());
    }

    #[test]
    fn read_error() {
        let path = PathBuf::from("/root/definitively/not/existing/path");
        let res = SomeData::load_from_file(&path);

        assert!(res.is_err());
    }
}
