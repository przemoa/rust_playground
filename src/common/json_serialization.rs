use std::{path::Path, time::SystemTime};

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct SomeData {
    pub id: u32,
    pub creation_time: SystemTime,
    pub img: Option<Vec<u8>>,
}

impl SomeData {
    pub fn new(img: &[u8]) -> Self {
        Self {
            id: 0,
            creation_time: SystemTime::now(),
            img: Some(img.to_vec()),
        }
    }

    pub fn save_to_file(&self, path: &Path) -> Result<(), Box<dyn std::error::Error>> {
        let json = serde_json::to_string_pretty(&self)?;
        std::fs::write(&path, json)?;
        Ok(())
    }

    pub fn load_from_file(path: &Path) -> Result<Self, Box<dyn std::error::Error>> {
        let json = std::fs::read_to_string(&path)?;
        let data = serde_json::from_str(&json)?;
        Ok(data)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::path::PathBuf;
    use tempfile::tempdir;

    #[test]
    fn write_to_file_and_read() {
        let dir = tempdir().expect("Failed to crate temp dir!");
        let path = PathBuf::from(dir.path()).join("test.bin");
        let data = SomeData::new(&[1, 2, 3, 4, 5]);

        data.save_to_file(&path).expect("Failed to save to file!");
        let loaded = SomeData::load_from_file(&path).expect("Failed to load from file!");

        assert_eq!(data, loaded);
    }
}
