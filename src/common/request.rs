pub fn download_data(url: &str) -> Result<String, Box<dyn std::error::Error>> {
    let mut response = reqwest::blocking::get(url)?;
    let response_text = response.text()?;
    Ok(response_text)
}
