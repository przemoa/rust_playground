// Just for early development, as everything is changing rapidly, so there is no point to warn about everything
#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(unused_mut)]
#![allow(unused_imports)]

use app_config::AppConfig;

mod app_config;
mod common;

fn init_logger(app_config: &AppConfig) {
    let res = log4rs::init_file(&app_config.log4rs_config_path, Default::default());

    match res {
        Ok(_) => log::info!("Logger initialized."),
        Err(e) => {
            let msg = format!(
                "CRITICAL ERROR! Failed to initialize logger: '{e}'. Continuing without logging..."
            );
            println!("{}", msg);
            eprintln!("{}", msg);
        }
    }
}

fn main() {
    let app_config = AppConfig::default();
    init_logger(&app_config);
    log::info!("Application started!");

    // Arc<Mutex<DataCollector>> - to share between threads
    


    log::info!("Application finished!");
}
