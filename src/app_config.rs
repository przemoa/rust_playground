use std::path::PathBuf;

#[derive(Clone, Debug)]
pub struct AppConfig {
    pub log4rs_config_path: PathBuf, // Path to the log4rs config file
}

impl AppConfig {
    /// Get the default application configuration for normal program run
    pub fn default() -> Self {
        Self {
            log4rs_config_path: PathBuf::from("config/log4rs.yml"),
        }
    }
}
